import os

from flask import Flask, request, make_response, redirect, abort, render_template, url_for, session, flash
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from datetime import datetime

from form_data import NameForm
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail, Message
from threading import Thread

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
app.config['SECRET_KEY'] = 'schuerä gheimä schlüssu däich'

Moment(app)
Bootstrap(app)

app.config['MAIL_SERVER'] = 'mail.cyon.ch'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
app.config['MAIL_USERNAME'] = os.environ.get('MAIL_USERNAME')
app.config['MAIL_PASSWORD'] = os.environ.get('MAIL_PASSWORD')
app.config['FLASKY_MAIL_SUBJECT_PREFIX'] = '[Flasky]'
app.config['FLASKY_MAIL_SENDER'] = 'Flasky Admin <flasky@example.com>'
mail = Mail(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

migrate = Migrate(app, db)


# from data_model import db
# db.drop_all()
# db.create_all()
# from data_model import User, Role
# role_admin = Role(name='Admin')
# role_mod = Role(name='Moderator')
# role_user = Role(name='User')
# user_john = User(name='John', role=role_admin)
# user_john = User(username='John', role=role_admin)
# user_susan = User(username='Susan', role=role_mod)
# user_david = User(username='David', role=role_user)
# db.session.add(role_admin)
# db.session.add(role_mod)
# db.session.add(role_user)
# db.session.add(user_john)
# db.session.add(user_susan)
# db.session.add(user_david)
# db.session.commit()

class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)
    users = db.relationship('User', backref='role')

    def __repr__(self):
        return '<Role %r>' % self.name


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)
    password = db.Column(db.String(64))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))

    def __repr__(self):
        return '<User %r>' % self.name


@app.route('/')
def index():  # put application's code here
    current_time = datetime.utcnow()
    return render_template('index.html', current_time=current_time)


@app.route('/user/<name>')
def user(name):
    # return f'Hello {name}', 400, {'killer': 'app'}
    error_page_url = url_for('error', x='9')
    return render_template('user.html', name=name, error_page_url=error_page_url)


@app.route('/test')
def test():
    return request.headers.get('User-Agent')


@app.route('/response')
def response():
    response = make_response('<H1>Result</H1>')
    response.headers.set('killer', 'app')
    response.status_code = 500
    return response


@app.route('/redirect')
def redirect_func():
    return redirect('/response')


@app.route('/error/<x>')
def error(x):
    if int(x) < 10:
        abort(404)
    return f'number {x} is valid'


# custom error page
@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.route('/form', methods=['GET', 'POST'])
def form():
    form = NameForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.name.data).first()
        if user is None:
            user = User(username=form.name.data)
            db.session.add(user)
            db.session.commit()
            session['known'] = False
        else:
            session['known'] = True
        session['name'] = form.name.data
        form.name.data = ''
        return redirect(url_for('form'))
    return render_template('form.html', form=form, name=session.get('name'))


@app.shell_context_processor
def make_shell_context():
    return dict(db=db, User=User, Role=Role)


def sendmail_sync(app, msg):
    with app.app_context():
        mail.send(msg)

def sendmail_async():
    msg = Message("Test Mail", sender="noreply@silvankrebs.com", recipients=["sik@silvankrebs.com"])
    msg.body = "This is a playn text body"
    msg.html = "<H1>HTML Body</H1>"
    thr = Thread(target=sendmail_sync, args=[app, msg])
    thr.start()
    return thr

@app.route('/sendmail')
def sendmail():
    sendmail_async()
    return "<H1>Mail sent</H1>"

if __name__ == '__main__':
    app.run()
